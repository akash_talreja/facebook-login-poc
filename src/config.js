let apiUrl = 'http://graph.facebook.com'
if (process.env.NODE_ENV === 'production') {
  apiUrl = ''
}
export {
  apiUrl
}
