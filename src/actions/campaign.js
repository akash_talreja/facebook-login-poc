import { SETIMAGE, SETTEXT } from '../types/campaign';

export function setImage(payload) {
  return {
    type: SETIMAGE,
    payload
  };
}

export function setText(payload) {
  return {
    type: SETTEXT,
    payload
  };
}
