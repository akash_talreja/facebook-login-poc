import { SET } from '../types/post';

export const set = (payload) => {
  return {
    type: SET,
    payload
  }
}
