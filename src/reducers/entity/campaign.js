import { SETIMAGE, SETTEXT } from '../../types/campaign';

const initialState = [
  {
    campaignName: 'Mom post',
    campaignId: 1,
    campaignText: '',
    campaignImage: '',
  },
  {
    campaignName: 'Late Night Party post',
    campaignId: 2,
    campaignText: '',
    campaignImage: '',
  },
  {
    campaignName: 'Work post',
    campaignId: 3,
    campaignText: '',
    campaignImage: '',
  }
]

export default function reducer(state=initialState, action) {
  switch (action.type) {
    case SETIMAGE: {
      const newState = [ ...state ];
      const campaign = { ...newState[action.payload.id - 1], campaignImage: action.payload.value };
      newState[action.payload.id - 1] = campaign
      return [ ...newState ];
    }
    case SETTEXT: {
      const newState = [ ...state ];
      const campaign = { ...newState[action.payload.id - 1], campaignText: action.payload.value };
      newState[action.payload.id - 1] = campaign
      return [ ...newState ];
    }
    default:
      return [ ...state ];
  }
}
