import { SET, RESET } from '../../types/user';

const initialState = {
  /*
  name: 'Akash Talreja',
    picture: {
      data: {
        is_silhouette: false,
        url: 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/11760205_572333852904880_1621375347209769193_n.jpg?oh=b4202189e03f305d4979564be0146596&oe=5A54DDD0'
      }
    },
    id: '938619502942978',
    accessToken: 'EAAbGYRPHUvEBACi3pi6dKoSqSpox877vkQAwwY1ycGsPWRQYfZAkVGcKiViSyaZB68EOgdYeV9nqjEJQ9YZCyWj2wlUiLhCMvdmP1gADbL7D8eIzjTPbILtw3GxLGVcUJqH4gorYLhLLPS2yQxtuBheOc68Xsqav9nOSgOzITnkJ0JvLMTfXQyTX3V8hXx5N8L3zll3cwZDZD',
    userID: '938619502942978',
    expiresIn: 5180,
    signedRequest: 'aPxFKwyiNpL41uSTSiXkzxJ03FS9hWIczP_GMJ-GzGg.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUNXT1NMaUJ5SU02cG80X3VabEpHOVNabEFWSlN6blg1WE8zUnU2QVRNb09uUGU3SE1pU29UbUhza2ZQdlBGeUlGUVBEQVlJei01WjE3dHBuLWZpQTJpZzlMX19kVUJCMGc5VV85ZVhaYmdNUlRLa2FXS004aS1fSWRSNzI4dDlEeEkyQ1RLRDNuQUMwVUUyTFJrUXd2UmtpVUpXQmRiaE9nSjR2NWo2R2V5eTNtTmR4RHRHZkRmdFU5UVV3VHdjdVR2Y2xOVHdHb21udHIzanZ2eGFKeVA1Q3g2dTlMR2h5cWFMbGI0dk5NdHRqRzVHRGxXTjdBUE10cHgtNU9uOGdhVnppdmU0TjRqZVNZalpMZGotZEU5d1FMcExEd1VlcV9IUnh1eUV6RG9SMHgweDhKSjZ4bzltUkJLaUkzM3E5S2x6dGNXaEVJVkRxeUh1X05aMjZpeiIsImlzc3VlZF9hdCI6MTUwNTM3ODAyMCwidXNlcl9pZCI6IjkzODYxOTUwMjk0Mjk3OCJ9'
    */
};

export default function reducer(state=initialState, action) {
  switch (action.type) {
    case SET:
      return {...state, ...action.payload}
    case RESET:
      return {...initialState}
    default:
      return state
  }
}
