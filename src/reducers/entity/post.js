import { SET } from '../../types/post';

const initialState = { };

export default function reducer(state=initialState, action) {
  switch (action.type) {
    case SET:
      return { ...state, ...action.payload };
    default:
      return { ...state };
  }
}
