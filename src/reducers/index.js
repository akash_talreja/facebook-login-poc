import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import user from './entity/user';
import campaign from './entity/campaign';
import post from './entity/post';

export default combineReducers({
  user,
  campaign,
  post,
  routing: routerReducer,
});
