import React from 'react';
import PropTypes from 'prop-types';

import './post-facebook-button.css';

import FacebookLogo from './../../resources/images/facebook-logo.svg';

const PostFacebookButton = ({ label, onClick }) => (
  <button className="post-facebook-button" onClick={onClick}>
    <div>
      <div>
        <img src={FacebookLogo} />
        <span>{label}</span>
      </div>
    </div>
  </button>
)

PostFacebookButton.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
};

export default PostFacebookButton;
