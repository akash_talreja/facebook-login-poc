import React from 'react';
import PropTypes from 'prop-types';

import './avatar.css';

const Avatar = ({ source }) => (
  <div className="avatar">
    <div className="avatar__image">
      <img src={ source } alt="campaign" />
    </div>
  </div>
)

Avatar.propTypes = {
  source: PropTypes.string,
};

export default Avatar;
