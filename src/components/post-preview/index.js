import React from 'react';
import PropTypes from 'prop-types';

import Avatar from './../avatar';

import './post-preview.css';

const PostPreview = ({ avatarImage, userName, contentText, contentImage }) => (
  <div className="post-preview">
    <div className="post-preview__header">
      <Avatar
        source={avatarImage}
      />
      <div className="campaign__header--user">
        <span>
          <strong>{userName}</strong>
        </span>
      </div>
    </div>
    <div className="post-preview__message">
      <span>{contentText}</span>
    </div>
    <div className="post-preview__content">
      <img src={contentImage} />
    </div>
  </div>
);

PostPreview.propTypes = {
  avatarImage: PropTypes.string,
  userName: PropTypes.string,
  contentText: PropTypes.string,
  contentImage: PropTypes.string
}

export default PostPreview;
