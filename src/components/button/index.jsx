import React from 'react';
import PropTypes from 'prop-types';

import './button.css';

const Button = ({ label, onClick }) => (
  <button className="button" onClick={onClick}>
    <div>
      <div>
        <span>{label}</span>
      </div>
    </div>
  </button>
)

Button.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
};

export default Button;
