import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import { connect } from 'react-redux';
import request from 'superagent';

import ImageUploader from 'react-images-upload';
import Avatar from './../avatar';
import Button from './../button';

import './campaign.css';

const Campaign = ({ user, campaign, setImage, setText, onSeePreviewClick }) => (
  <div className="campaign">
    <div className="campaign__name">
      {
        campaign.campaignName
      }
    </div>
    <div className="campaign__header">
      <Avatar
        source={user.picture.data.url}
      />
      <div className="campaign__header--user">
        <span>
          <strong>{user.name}</strong>
        </span>
      </div>
    </div>
    <div className="campaign__content">
      <div className="campaign__message">
        <input type="text" placeholder="Enter message" onChange={ (event) => { setText({ id: campaign.campaignId, value: event.target.value }) } } />
      </div>
      <div className="campaign__image">
        <input type="text" placeholder="Enter Image Url" onChange={ (event) => { setImage({ id: campaign.campaignId, value: event.target.value }) } } />
      </div>
      <div className="campaign__button">
        <Button
          label="see preview"
          onClick={() => onSeePreviewClick(campaign.campaignId)}
        />
      </div>
    </div>
  </div>
)

Campaign.propTypes = {
  user: PropTypes.object,
  campaign: PropTypes.object,
  setImage: PropTypes.func,
  setText: PropTypes.func,
  onSeePreviewClick: PropTypes.func
}

export default Campaign;
