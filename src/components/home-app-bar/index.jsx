import React from 'react';
import './home-app-bar.css';

const HomeAppBar = () => (
  <div className="home-app-bar">
    <button className="home-app-bar__button">
      <div>
        <svg viewBox="0 0 24 24" className="home-app-bar__button--menu">
          <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path>
        </svg>
      </div>
    </button>
    <span className="home-app-bar__title">
      uber
    </span>
  </div>
);

export default HomeAppBar;
