import React from 'react';
import './campaigns-app-bar.css';

const CampaignsAppBar = () => (
  <div className="campaigns-app-bar">
    <button className="campaigns-app-bar__button">
      <div>
        <svg viewBox="0 0 24 24" className="campaigns-app-bar__button--back">
          <path d="M0 0h24v24H0z" fill="none"/>
          <path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/>
        </svg>
      </div>
    </button>
    <div className="campaigns-app-bar__title">
      <span>uber &nbsp;</span>
      <span>influencers</span>
    </div>
  </div>
);

export default CampaignsAppBar;
