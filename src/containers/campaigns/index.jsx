import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { setText, setImage } from './../../actions/campaign';
import { set as setPost } from './../../actions/post';

import CampaignsAppBar from './../../components/campaigns-app-bar';
import Campaign from './../../components/campaign';

import './campaigns.css';

class Campaigns extends Component {
  constructor(props) {
    super(props);

    this.onSeePreviewClick = this.onSeePreviewClick.bind(this);
  }

  onSeePreviewClick(id) {
    const payload = {
      caption: this.props.campaigns[id - 1].campaignText,
      imageUrl: this.props.campaigns[id - 1].campaignImage
    }
    this.props.setPost(payload);
    this.props.router.push('/preview');
  }

  render() {
    return (
      <div className="campaigns">
        <CampaignsAppBar />
        <div className="campaigns__content">
          {
            this.props.campaigns.map(campaign => (
              <Campaign
                key={campaign.campaignId}
                user={this.props.user}
                campaign={campaign}
                setImage={this.props.setImage}
                setText={this.props.setText}
                onSeePreviewClick={this.onSeePreviewClick}
              />
            ))
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    campaigns: state.campaign,
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setImage: (payload) => { dispatch(setImage(payload)) },
    setText: (payload) => { dispatch(setText(payload)) },
    setPost: (payload) => { dispatch(setPost(payload)) },
  }
}

Campaigns.propTypes = {
  campaigns: PropTypes.array,
  user: PropTypes.object,
  setImage: PropTypes.func,
  setText: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(Campaigns);
