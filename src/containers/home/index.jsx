import React, { Component } from 'react';
import { connect } from 'react-redux';

import HomeAppBar from './../../components/home-app-bar';
import Button from './../../components/button';

import HomeImage from './../../resources/images/home-image.jpg';
import './home.css';

class Home extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    if (this.props.users !== undefined) {
      this.props.router.push('/campaigns');
    }
  }

  render() {
    return(
      <div className="home">
        <HomeAppBar />
        <div className="home__description">
          <span>YOU be the new</span>
          <span>UBER <span>brand ambassador</span></span>
        </div>
        <div className="home__image">
          <img src={HomeImage} alt="hero"/>
        </div>
        <div className="home__message">
          Share your favourite UBER story on Facebook & become the newest UBER brand ambassador
        </div>
        <div className="home__button">
          <Button
            label="get started"
            onClick={() => this.props.router.push('/login')}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(Home);
