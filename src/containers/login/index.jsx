import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { set } from './../../actions/user';
import HomeAppBar from './../../components/home-app-bar';

import './login.css';

class Login extends Component {
  constructor(props) {
    super(props);
    this.responseCallback = this.responseCallback.bind(this);
  }

  responseCallback(response) {
    console.log(response);
    this.props.setUser(response);
    this.props.router.push('/campaigns');
  }

  render() {
    return (
      <div className="login">
        <HomeAppBar />
        <div className="login__content">
          <div className="login__description">
            login with Facebook to continue
          </div>
          <div className="login__button">
            <FacebookLogin
              appId="1906970106221297"
              autoload={true}
              fields="name,email,picture"
              scope="publish_actions"
              callback={this.responseCallback}
            />
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: response => { dispatch(set(response)) },
  }
}

Login.propTypes = {
  setUser: PropTypes.func,
  router: PropTypes.object,
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
