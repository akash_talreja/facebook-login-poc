import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { postOnFacebook } from './../../api-callbacks/post';

import CampaignsAppBar from './../../components/campaigns-app-bar';
import PostPreview from './../../components/post-preview';
import PostFacebookButton from './../../components/post-facebook-button';

import './preview.css';

class Preview extends Component {
  constructor(props) {
    super(props);
    this.postOnFacebookClick = this.postOnFacebookClick.bind(this);
  }

  postOnFacebookClick() {
    const id = this.props.user.id;
    const queryParams = {
      access_token: this.props.user.accessToken,
      caption: this.props.post.caption,
      url: this.props.post.imageUrl
    };
    this.props.photoPostOnFacebook(id, queryParams);
  }

  render() {
    return (
      <div className="preview">
        <CampaignsAppBar />
        <div className="preview__content">
          <PostPreview
            avatarImage={this.props.user.picture.data.url}
            userName={this.props.user.name}
            contentText={this.props.post.caption}
            contentImage={this.props.post.imageUrl}
          />
          <div className="preview__content--button">
            <PostFacebookButton
              label="Post on Facebook"
              onClick={this.postOnFacebookClick}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    post: state.post
  };
};

const mapDispatchToProps = dispatch => {
  return {
    photoPostOnFacebook: (id, queryParams) => { dispatch(postOnFacebook(id, queryParams)) }
  };
};

Preview.propTypes = {
  user: PropTypes.object,
  post: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(Preview);
