import request from 'superagent';

import { apiUrl } from './../config';

export const postOnFacebook = (id, queryParams) => {

  return (dispatch, getState) => {
    request
    .post(`${apiUrl}/${getState().user.id}/photos`)
    .query({
        access_token: getState().user.accessToken,
        caption: getState().post.message,
        url: getState().post.imageUrl
      })
    .end((err, res) => {
      if(err) {
        console.log("err");
      }
      console.log("success");
    })
  }
}
