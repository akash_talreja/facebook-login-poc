import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import Home from './containers/home/';
import Login from './containers/login';
import Campaigns from './containers/campaigns';
import Preview from './containers/preview';

import configureStore from './store';
import './index.css';
import App from './containers/app';
import registerServiceWorker from './registerServiceWorker';
import './../node_modules/normalize.css/normalize.css';

// Let the reducers handle initial state
const initialState = {};

const store = configureStore(initialState);
const history = syncHistoryWithStore(browserHistory, store);

history.listen((location) => {
  const path = (/#(\/.*)$/.exec(location.hash) || [])[1];
  if (path) history.replace(path);
});


ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/campaigns" component={Campaigns} />
        <Route path="/preview" component={Preview} />
      </Route>
    </Router>
  </Provider>
, document.getElementById('root')
)
registerServiceWorker()
